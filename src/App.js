import { useState } from 'react';
import './App.scss'; 
import Button from "./components/Button"; 
import Modal from "./components/Modal";

function App() {
    const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
    const [isAddModalOpen, setIsAddModalOpen] = useState(false);

    function openDeleteModal(){
        setIsDeleteModalOpen(true);
    }

    function openAddModal(){
        setIsAddModalOpen(true);
    }


    function closeModal(){
        setIsDeleteModalOpen(false);
        setIsAddModalOpen(false);
    }

    return (
        <div className="App">
            {isDeleteModalOpen && 
                <Modal
                backgroundColor='linear-gradient(to bottom, #D44638 24%, #E74C3D 24% 100%)'
                header='Do you want to delete this file?'
                closeButton='true'
                // closeButton='false'
                text='Once you delete this file it won`t be possible to undo this action.
                Are you sure you want to delete it?'
                actions={<><button className='buttonForDeleteModal'>Ok</button>
                <button className='buttonForDeleteModal'>Cancel</button></>}
                closeModal={closeModal}
            />
            }

            {isAddModalOpen && 
                <Modal
                backgroundColor='linear-gradient(to bottom, #348a0c 24%, #269618 24% 100%)'
                header='Do you want to add new file?'
                closeButton='true'
                // closeButton='false'
                text='If you choose No you, you can still add it later manually'
                actions={<><button className='buttonForAddModal'>Add</button>
                <button className='buttonForAddModal'>No</button></>}
                closeModal={closeModal}
            />
            }
            
            <div class="modalButtonsWrapper">
                <Button
                    backgroundColor='#E74C3D'
                    text='Open first modal'
                    onClick={openDeleteModal}
                />
                <Button
                    backgroundColor='#269618'
                    text='Open second modal'
                    onClick={openAddModal}
                />
            </div>
        </div>
    );
}

export default App;
